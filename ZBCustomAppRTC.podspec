Pod::Spec.new do |s|

	s.name             = "ZBCustomAppRTC"
	s.version          = "1.0.0"
	s.summary          = "Modified lib of App RTC https://bitbucket.org/Maxvale/zbcustomapprtc"

	s.description      = <<-DESC
	Logic behind offers for internal projects.
	DESC

	s.homepage         = "https://bitbucket.org/Maxvale/zbcustomapprtc"
	s.license          = { :type => "BSD", :file => "LICENSE" }
	s.author           = { "Zmicier Biesau" => "zmicier.biesau@gmail.com" }
	s.source           = { :git => "https://bitbucket.org/Maxvale/zbcustomapprtc.git", :tag => s.version.to_s }
	s.platform     = :ios, '8.0'
	s.requires_arc = true
	s.source_files = 'Pod/Classes/**/*'
	s.requires_arc       = true
  	s.frameworks         = "QuartzCore", "OpenGLES", "CoreGraphics", "CoreVideo", "CoreMedia", "CoreAudio", "AVFoundation", "AudioToolbox", "GLKit", "CFNetwork", "Security"
  	s.libraries          = "sqlite3", "stdc++.6", "icucore", "c++"
  	s.dependency "libjingle_peerconnection"
  	s.dependency "SocketRocket"
end
