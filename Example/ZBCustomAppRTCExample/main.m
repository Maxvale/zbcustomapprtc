//
//  main.m
//  ZBCustomAppRTCExample
//
//  Created by Źmicier Biesau on 16.08.2016.
//  Copyright © 2016 Maxvale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
